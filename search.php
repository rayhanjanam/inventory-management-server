<?php

class Search
{
    private $connection;

    public function __construct()
    {
        $db = Database::getInstance();
        $this->connection = $db->getConnection();
    }

    public function performSearch($searchString)
    {
        $string = $searchString . '%';
        $query = "SELECT * FROM purchase WHERE productName LIKE ?";

        $rows = array();

        if ($statement = $this->connection->prepare($query)) {
            $statement->bind_param('s', $string);
            $statement->execute();
            $statement->bind_result($id, $batch, $name, $unit, $price, $date, $branch, $rol);

            while ($statement->fetch()) {

                if ($rol == null) {
                    $rol = 0;
                }

                $row = array('id' => $id, 'batch' => $batch, 'productName' => $name, 'unit' => $unit, 'price' => $price, 'date' => $date, 'branch' => $branch, 'rol' => $rol);

                $rows[] = $row;
            }

            return Message::create(10, 'success', $rows);
        }

        return Message::create(999, 'Operation failed, invalid state');
    }

    public function fetchAll()
    {
        $rows = array();
        $query = "SELECT * FROM purchase";

        if ($statement = $this->connection->prepare($query)) {
            $statement->execute();
            $statement->bind_result($id, $batch, $name, $unit, $price, $date, $branch, $rol);

            while ($statement->fetch()) {

                if ($rol == null) {
                    $rol = 0;
                }

                $row = array('id' => $id, 'batch' => $batch, 'productName' => $name, 'unit' => $unit, 'price' => $price, 'date' => $date, 'branch' => $branch, 'rol' => $rol);

                $rows[] = $row;
            }

            return Message::create(10, 'success', $rows);
        }

        return Message::create(999, 'Operation failed, invalid state');
    }
}
