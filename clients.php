<?php

class Clients
{
    private $id;
    private $name;
    private $address;
    private $phone;
    private $email;
    private $type;

    private $connection;

    public function __construct($id, $name, $address, $phone, $email, $type)
    {
        $this->id = $id;
        $this->name = $name;
        $this->address = $address;
        $this->phone = $phone;
        $this->type = $type;
        $this->email = $email;

        $db = Database::getInstance();
        $this->connection = $db->getConnection();
    }

    public function All()
    {
        $rows = array();
        $query = "SELECT * FROM clients";

        if ($stmt = $this->connection->prepare($query)) {
            $stmt->execute();
            $stmt->bind_result($id, $name, $address, $phone, $email, $type);

            while ($stmt->fetch()) {
                $row = array('id' => $id, 'name' => $name, 'address' => $address, 'phone' => $phone, 'email' => $email, 'type' => $type);
                $rows[] = $row;
            }

            return Message::create(10, 'success', $rows);
        }

        return Message::create(999, 'Operation failed, invalid state');
    }

    public function Insert()
    {
        $query = "INSERT INTO clients (id, name, address, phone, email, type) VALUES (NULL,?,?,?,?,?)";

        if ($statement = $this->connection->prepare($query)) {
            $statement->bind_param('sssss', $this->name, $this->address, $this->phone, $this->email, $this->type);

            if ($statement->execute()) {
                return Message::create(10, 'Insertion success');
            }
        }

        return Message::create(999, 'Operation failed, invalid state');
    }

    public function Update()
    {
        $query = "UPDATE clients SET name=?, address=?, phone=?, email=?, type=? WHERE id=?";

        if ($statement = $this->connection->prepare($query)) {
            $statement->bind_param('sssssi', $this->name, $this->address, $this->phone, $this->email, $this->type, $this->id);

            if ($statement->execute()) {
                return Message::create(10, 'update success');
            }
        }

        return Message::create(999, 'Operation failed, invalid state');
    }

    public function Delete()
    {
        $query = "DELETE FROM clients WHERE id=?";

        if ($statement = $this->connection->prepare($query)) {
            $statement->bind_param('i', $this->id);

            if ($statement->execute()) {
                return Message::create(10, 'deletion success');
            }
        }

        return Message::create(999, 'Operation failed, invalid state');
    }
}
