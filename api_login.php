<?php

require 'login.php';

if (isset($_POST['username']) && isset($_POST['password'])) {

    $uname = $_POST['username'];
    $passwd = $_POST['password'];

    $login = new Login($uname, $passwd);
    $result = $login->performLogin();

    exit(json_encode($result));

} else {
    exit(json_encode(Message::create(3, 'Operation Failed, invalid Request')));
}
