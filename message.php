<?php

class Message
{
    public static $tagCode = 'code';
    public static $tagMessage = 'message';
    public static $tagData = 'data';

    public static function create($code, $message, $data = null)
    {

        $message = array(Message::$tagCode => $code, Message::$tagMessage => $message, Message::$tagData => $data);

        return $message;
    }
}
