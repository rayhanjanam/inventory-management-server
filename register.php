<?php

require 'db.php';

if (isset($_POST['passcode']) && isset($_POST['username']) && isset($_POST['password']) && isset($_POST['password2'])) {

    $passcode = $_POST['passcode'];
    $username = $_POST['username'];
    $password = $_POST['password'];
    $password2 = $_POST['password2'];

    if ($passcode != '' && $username != '' && $password != '' && $password2 != '') {
        if ($password == $password2) {
            if ($passcode == 'admin001') {

                $hashedPassword = sha1($password);
                $uid = uniqid();

                // database entry

                $db = Database::getInstance();
                $connection = $db->getConnection();

                if ($statement = $connection->prepare("INSERT INTO users (user_id, username, password) VALUES (?, ?, ?)")) {
                    $statement->bind_param('sss', $uid, $username, $hashedPassword);

                    if ($statement->execute()) {
                        echo '<h2>User created successfully</h2>';
                    } else {
                        echo '<h2>Insertion Failed, user not created</h2>';
                    }
                }

            } else {
                echo "<h2>WRONG PASSCODE</h2>";
            }
        } else {
            echo '<h2>Passwords do not match</h2>';
        }
    } else {
        echo '<h2>Empty fields</h2>';
    }
} else {
    echo '<h2>Invalid request</h2>';
}
