<?php

require 'message.php';

class Database
{
    private static $instance;

    private $server_name = 'localhost';
    private $databse_username = 'root';
    private $database_password = '';
    private $database_name = 'inventory_management';

    private function __construct()
    {}

    public static function getInstance()
    {

        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getConnection()
    {

        $connection = new mysqli($this->server_name, $this->databse_username, $this->database_password, $this->database_name);

        if ($connection->connect_error) {
            echo json_encode(Message::create(999, 'Operation failed, database not available'));
            die();
        }

        return $connection;
    }
}
