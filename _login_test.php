<!DOCTYPE html>
<html lang="en">
<head>
    <title>IMP - Login</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container" style="width: 500px;">

    <br>
    <h2>Inventory Management Program</h2>
    <form action="/Inventory Management Server/api_login.php" method="post">

    <br>
    <br>

    <div class="form-group">
      <label for="username">Username:</label>
      <input type="text" class="form-control" id="username" placeholder="Enter username" name="username">
    </div>

    <div class="form-group">
      <label for="password">Password:</label>
      <input type="password" class="form-control" id="password" placeholder="Enter password" name="password">
    </div>

    <br>

    <div class="col-sm-12 text-center">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
</div>

</body>
</html>
