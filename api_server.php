<?php

require 'purchase.php';
require 'search.php';
require 'sales.php';
require 'rol.php';
require 'clients.php';

if (isset($_GET['mode'])) {
    $mode = $_GET['mode'];

    if ($mode == 'search') {

        if (isset($_POST['query'])) {
            $searchString = $_POST['query'];
            $search = new Search();
            $result = $search->performSearch($searchString);

            exit(json_encode($result));
        }

        exit(json_encode(Message::create(999, 'Invalid request')));

    } elseif ($mode == 'list') {

        $search = new Search();
        $result = $search->fetchAll();

        exit(json_encode($result));

    } elseif ($mode == 'purchase') {

        if (isset($_POST['name']) && isset($_POST['unit']) && isset($_POST['price']) && isset($_POST['date']) && isset($_POST['branch'])) {
            $name = $_POST['name'];
            $unit = $_POST['unit'];
            $price = intval($_POST['price']);
            $date = $_POST['date'];
            $branch = $_POST['branch'];

            $purchase = new Purchase($name, $unit, $price, $date, $branch);
            $result = $purchase->performPurchase();

            exit(json_encode($result));
        }

        exit(json_encode(Message::create(999, 'Invalid request')));

    } elseif ($mode == 'sales') {

        if (isset($_POST['batch']) && isset($_POST['unit']) && isset($_POST['price']) && isset($_POST['leadtime']) && isset($_POST['date'])) {
            $batch = $_POST['batch'];
            $unit = intval($_POST['unit']);
            $price = $_POST['price'];
            $leadtime = $_POST['leadtime'];
            $date = $_POST['date'];

            $sales = new Sales($batch, $unit, $price, $leadtime, $date);
            $result = $sales->performSales();

            exit(json_encode($result));
        }

        exit(json_encode(Message::create(999, 'Invalid request')));

    } elseif ($mode == 'setrol') {

        if (isset($_POST['batch']) && isset($_POST['rol'])) {
            $batch = $_POST['batch'];
            $rol = intval($_POST['rol']);

            $rols = new ROL($batch, $rol);
            $result = $rols->addRolData();

            exit(json_encode($result));
        }

        exit(json_encode(Message::create(999, 'Invalid request')));

    } elseif ($mode == 'clientadd') {

        if (isset($_POST['name']) && isset($_POST['address']) && isset($_POST['phone']) && isset($_POST['email']) && isset($_POST['type'])) {
            $name = $_POST['name'];
            $address = $_POST['address'];
            $phone = $_POST['phone'];
            $email = $_POST['email'];
            $type = $_POST['type'];

            $client = new Clients(null, $name, $address, $phone, $email, $type);
            $result = $client->Insert();

            exit(json_encode($result));
        }

        exit(json_encode(Message::create(999, 'Invalid request')));

    } elseif ($mode == 'clientupdate') {

        if (isset($_POST['id']) && isset($_POST['name']) && isset($_POST['address']) && isset($_POST['phone']) && isset($_POST['email']) && isset($_POST['type'])) {
            $id = $_POST['id'];
            $name = $_POST['name'];
            $address = $_POST['address'];
            $phone = $_POST['phone'];
            $email = $_POST['email'];
            $type = $_POST['type'];

            $client = new Clients($id, $name, $address, $phone, $email, $type);
            $result = $client->Update();

            exit(json_encode($result));
        }

        exit(json_encode(Message::create(999, 'Invalid request')));

    } elseif ($mode == 'clientdelete') {

        if (isset($_POST['id'])) {
            $id = $_POST['id'];

            $client = new Clients($id, null, null, null, null, null);
            $result = $client->Delete();

            exit(json_encode($result));
        }

        exit(json_encode(Message::create(999, 'Invalid request')));

    } elseif ($mode == 'clientlist') {

        $client = new Clients(null, null, null, null, null, null);
        $result = $client->All();

        exit(json_encode($result));
    } else {
        exit(json_encode(Message::create(999, 'Invalid request')));
    }
} else {
    exit(json_encode(Message::create(999, 'Error processing request')));
}
