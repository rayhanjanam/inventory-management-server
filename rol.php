<?php

class ROL
{
    private $batch;
    private $rol;

    private $connection;

    public function __construct($batch, $rol)
    {
        $this->batch = $batch;
        $this->rol = $rol;

        $db = Database::getInstance();
        $this->connection = $db->getConnection();
    }

    public function addRolData()
    {
        $query = "UPDATE purchase SET rol=? WHERE batch=?";

        if ($stmt = $this->connection->prepare($query)) {
            $stmt->bind_param('is', $this->rol, $this->batch);

            if ($stmt->execute()) {
                return Message::create(10000, 'Updated ROL');
            }
        }

        return Message::create(999, 'Invalid State');
    }
}
