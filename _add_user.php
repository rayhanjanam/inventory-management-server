<!DOCTYPE html>
<html lang="en">
<head>
  <title>IMP - Add User</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container" style="width: 500px;">
  <h2>Inventory Management Program</h2>
  <form action="/Inventory Management Server/register.php" method="post">

    <p>Create new user for Inventory Management Program</p>

    <div class="form-group">
      <label for="passcode">PASSCODE:</label>
      <input type="text" class="form-control" id="passcode" placeholder="Enter passcode" name="passcode">
    </div>

    <div class="form-group">
      <label for="username">Username:</label>
      <input type="text" class="form-control" id="username" placeholder="Choose a username" name="username">
    </div>

    <div class="form-group">
      <label for="password">Password:</label>
      <input type="password" class="form-control" id="password" placeholder="Choose a password" name="password">
    </div>

    <div class="form-group">
      <label for="password2">Re-enter Password:</label>
      <input type="password" class="form-control" id="password2" placeholder="Enter password again" name="password2">
    </div>

    <br>

    <div class="col-sm-12 text-center">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
</div>

</body>
</html>
