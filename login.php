<?php

require 'db.php';

class Login
{

    private $username;
    private $password;
    private $connection;

    public function __construct($uname, $passwd)
    {

        $this->username = $uname;
        $this->password = $passwd;

        $db = Database::getInstance();
        $this->connection = $db->getConnection();
    }

    private function usernameExists($uname)
    {

        // check for username

        if ($statement = $this->connection->prepare("SELECT user_id FROM users WHERE username=?")) {
            $statement->bind_param("s", $uname);
            $statement->execute();
            $statement->bind_result($uid);
            $statement->fetch();

            if ($uid != "") {
                return true;
            }
        }

        return false;
    }

    public function performLogin()
    {

        if ($this->usernameExists($this->username) == true) {

            // login, check password

            if ($statement = $this->connection->prepare("SELECT password FROM users WHERE username=?")) {
                $statement->bind_param("s", $this->username);
                $statement->execute();
                $statement->bind_result($passwordDb);
                $statement->fetch();

                if (sha1($this->password) == $passwordDb) {
                    return Message::create(0, 'Login success');
                }

                return Message::create(1, 'Login failure, wrong password');
            }

            return Message::create(999, 'Operation failed, invalid state');

        } else {
            return Message::create(2, 'Login failure, username does not exist');
        }

    }

}
