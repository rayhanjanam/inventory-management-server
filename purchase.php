<?php

require 'db.php';

class Purchase
{
    private $productBatch;
    private $productName;
    private $units;
    private $purchasePrice;
    private $purchaseDate;
    private $branch;

    private $connection;

    public function __construct($name, $unit, $price, $date, $branch)
    {
        $uid = uniqid();
        $ext = substr($name, 0, 3);

        $this->productBatch = $ext . $uid;
        $this->productName = $name;
        $this->units = $unit;
        $this->purchasePrice = $price;
        $this->purchaseDate = $date;
        $this->branch = $branch;

        $db = Database::getInstance();
        $this->connection = $db->getConnection();
    }

    public function performPurchase()
    {
        $query = "INSERT INTO purchase (id, batch, productName, unit, purchasePrice, purchaseDate, branch, rol) VALUES (NULL,?,?,?,?,?,?,NULL)";

        if ($statement = $this->connection->prepare($query)) {
            $statement->bind_param('ssisss', $this->productBatch, $this->productName, $this->units, $this->purchasePrice, $this->purchaseDate, $this->branch);

            if ($statement->execute()) {
                return Message::create(100, $this->productBatch);
            }
        }

        return Message::create(999, 'Operation failed, invalid state');
    }
}
