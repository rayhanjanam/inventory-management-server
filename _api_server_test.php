<?php

require 'purchase.php';
require 'search.php';
require 'sales.php';

if (isset($_GET['mode'])) {
    $mode = $_GET['mode'];

    if ($mode == 'search') {

        $search = new Search('CAS');
        $result = $search->performSearch();

        exit(json_encode($result));

    } elseif ($mode == 'list') {

        $search = new Search();
        $result = $search->fetchAll();

        exit(json_encode($result));

    } elseif ($mode == 'purchase') {

        $purchase = new Purchase('CASIO-FX991ES', 10, '1300', '30.03.2018', 'DHAKA');
        $result = $purchase->performPurchase();

        exit(json_encode($result));

    } elseif ($mode == 'sales') {

        $sales = new Sales('CAS5abe69d207b7d', 3, '1500', '0.5', '12.05.2018');
        $result = $sales->performSales();

        exit(json_encode($result));

    } else {
        exit(json_encode(Message::create(11, 'Invalid Request')));
    }
} else {
    exit(json_encode(Message::create(12, 'Error Processing Request')));
}
