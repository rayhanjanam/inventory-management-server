<?php

class Sales
{
    private $productBatch;
    private $productName;
    private $units;
    private $unitsForSale;
    private $salesPrice;
    private $leadTime;
    private $salesDate;

    private $reorderPoint;

    private $connection;

    public function __construct($batch, $unit, $price, $lead_time, $date)
    {
        $this->productBatch = $batch;
        $this->unitsForSale = $unit;
        $this->salesPrice = $price;
        $this->leadTime = $lead_time;
        $this->salesDate = $date;
        $this->reorderPoint = 2;

        $db = Database::getInstance();
        $this->connection = $db->getConnection();
    }

    public function performSales()
    {
        // get purchase detail

        $purchaseDetail = array();
        $query = "SELECT * FROM purchase WHERE batch=?";

        if ($stmt = $this->connection->prepare($query)) {
            $stmt->bind_param('s', $this->productBatch);
            $stmt->execute();
            $stmt->bind_result($id, $batch, $name, $unit, $price, $date, $branch, $rol);
            $stmt->fetch();
            $stmt->close();

            if ($rol == null)
            {
                $rol = 0;
            }

            $this->reorderPoint = $rol;

            if ($id != '') {

                if ($unit > $this->unitsForSale) {

                    // insert into sales table

                    $query = "INSERT INTO sales (id, batch, productName, unit, salesPrice, salesDate, leadTime, branch) VALUES (NULL,?,?,?,?,?,?,?)";

                    if ($statement = $this->connection->prepare($query)) {
                        $statement->bind_param('ssissss', $batch, $name, $this->unitsForSale, $this->salesPrice, $this->salesDate, $this->leadTime, $branch);

                        if ($statement->execute()) {
                            $statement->close();

                            // update purchase table

                            $query = "UPDATE purchase SET unit=? WHERE batch=?";
                            $unitToUpdate = $unit - $this->unitsForSale;

                            if ($statement = $this->connection->prepare($query)) {
                                $statement->bind_param('is', $unitToUpdate, $this->productBatch);

                                if ($statement->execute()) {

                                    // success

                                    if ($unit - $this->unitsForSale <= $this->reorderPoint) {

                                        return Message::create(1001, 'Sales success, Re-order is needed');
                                    }

                                    return Message::create(1000, 'Sales success');
                                }
                            }

                            return Message::create(999, 'Operation failed, invalid state');
                        }
                    }

                    return Message::create(999, 'Operation failed, invalid state');
                }

                return Message::create(1002, 'Operation failed, not enough product in store');
            }

            return Message::create(1003, 'Operation failed, no record found');
        }

        return Message::create(999, 'Operation failed, invalid state');
    }
}
